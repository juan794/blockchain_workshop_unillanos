# blockchain_workshop_unillanos

To install packages required for the exercises, execute the following commands:

```shell
pip3 install -r $REPO_DIR/requirements.txt
```

To connect to the VPN, use any of the files in $REPO_DIR/vpn/

```shell
sudo openvpn --config minerX.ovpn
```