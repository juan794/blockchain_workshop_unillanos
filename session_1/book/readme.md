# Reto para Libro
- Dificultad de minado de 26 bits
- Bloque Genesis es '51d5664ef15bbad7e4f2acd595faf5a213980b4c274becd440911516312ea9ec'
- Implementar Merkle Tree en el minado de bloques
- Si el numero de hojas (leaf), basado en las transacciones, es mayor que uno (1) y no es múltiplo de dos (2), agregar una hoja con string vacio ("")
- En la generación del Merkle Tree debe usar los valores hash en **Bytes**, y unicamente convertir a String el valor hash del Root (el cual se incluye en la cabecera del block (Block Header > txhash)
- Recuerde que para calcular el hash de una transacción, hay que convertir el objeto a JSON primero
- Cada transacción tiene que estar validada:
	- Origen (src) tenga fondos suficientes para realizar la transacción
    - Validar la firma digital (signature), usando ECDSA con NIST256p SHA256
- Ejemplos de Bloques en formato JSON en [archivo1](blocks_example.json) y [archivo2](blocks_example2.json)


# Oráculo 
- El oráculo ha sido modificado para generar transacciones. Ejecutar de la siguiente manera:

    - Primero, ejecutar el oráculo en modo minero
    ```shell
    ./oracle_x86_64.bin --config miner.yml
    ```

    - Segundo, en otra consola, ejecutar el oráculo en modo generador de transacciones
    ```shell
    ./oracle_x86_64.bin --config tx_generator.yml
    ```

    - Ahora puede conectar su minero al oráculo a traves de **localhost:5555**

# Entregables
- JSON de los bloques minados (al menos 5 bloques)
- Los bloques deben tener transacciones generadas por el oráculo
- El valor txhash debe corresponder al Merkle Tree Root de las transacciones
- Todas las transacciones deben ser validas
- Entregar antes del 18 de Diciembre 2020
- Los JSON serán validados para determinar el ganador
- Primero en entregar, gana el libro: **Mastering Blockchain, escrito por Imran Bashir (libro valorado en COP$200,000 aprox.)**

# Esquema de Ejemplo 
```
BLOCK N:
     +----------------------+  --+
     |        Nonce (uint32)|    |
     +----------------------+    |
     |         Time (uint32)|    |--(JSON)---(HASH)
     +----------------------+    |             |
     |  Previous Block Hash |    |             |
     +----------------------+    |             |
     |   Merkle Tree Root   |    |             |
     ++++++++++++++++++++++++  --+             |
     |          TX0         |  ----------------|------->
     +----------------------+                  |          Transaction Tx0:
                                               |          +----------------------+
                                               |          |        Nonce         |
                                               |          +----------------------+
                                               |          |        Source        |
                                               |          +----------------------+
                                               |          |     Destination      |
                                               |          +----------------------+
BLOCK N+1:                                     |          |        Amount        |
     +----------------------+                  |          +----------------------+
     |        Nonce         |                  |          |       Signature      |
     +----------------------+                  |          +----------------------+
     |         Time         |                  |
     +----------------------+                  |
     |  Previous Block Hash |  <---------------+
     +----------------------+
     |   Merkle Tree Root   |  <----(HEX to String)---------------+
     ++++++++++++++++++++++++                                     |
     |          Tx0         |  --(JSON)--(H)--+                   |
     +----------------------+                 |--(H)--+           |
     |          Tx1         |  --(JSON)--(H)--+       |           |
     +----------------------+                         |--(H)--+   |
     |          Tx2         |  --(JSON)--(H)--+       |       |   |
     +----------------------+                 |--(H)--+       |   |
     |          ...         |  --(JSON)--(H)--+               |   |
     +----------------------+                                 |--(H)  <-- ROOT
     |          TxN         |  --(JSON)--(H)--+               |
     +----------------------+                 |--(H)--+       |
     |         TxN+1        |  --(JSON)--(H)--+       |       |
     +----------------------+                         |--(H)--+
     |         TxN+2        |  --(JSON)--(H)--+       |
     +----------------------+                 |--(H)--+
                               --( "" )--(H)--+
```

